import * as t from "../types";

const mainReducer = (state = {
  loading: false,
  list: [],
  search: '',
  history: [],
  page: 1,
}, action) => {
  switch(action.type){
    case t.DO_SEARCH:
      let history = [...state.history];
      if (history.includes(action.payload)) {
        const index = history.findIndex(item => item === action.payload);
        history.splice(index, 1);
      } else {
        history.slice(0, 9);
      }
      history = [action.payload, ...history];
      return {
        ...state,
        page: 1,
        history,
        search: action.payload
      };
    case t.SET_PAGE:
      return { 
        ...state,
        page: action.payload
      };
    case t.SET_LOADING:
      return { 
        ...state,
        loading: action.payload
      };
    case t.SET_LIST:
      return { 
        ...state,
        list: action.payload
      };
    case t.SET_SEARCH_TEXT:
      return { 
        ...state,
        search: action.payload
      };
    case t.SET_SEARCH_HISTORY:
      return { 
        ...state,
        history: action.payload
      };
    default:
      return {...state};
    }
}

export default mainReducer;