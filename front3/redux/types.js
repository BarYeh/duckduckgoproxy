export const SET_NAME = 'SET_NAME';
export const SET_LOADING = 'SET_LOADING';
export const SET_LIST = 'SET_LIST';
export const SET_SEARCH_HISTORY = 'SET_SEARCH_HISTORY';
export const SET_SEARCH_TEXT = 'SET_SEARCH_TEXT';
export const SET_PAGE = 'SET_PAGE';
export const DO_SEARCH = 'DO_SEARCH';
