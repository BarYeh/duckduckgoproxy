import * as t from "../types";

export const setLoading = (loading) => dispatch => {
  dispatch({
    type: t.SET_LOADING,
    payload: loading
  });
}

export const setList = (list) => dispatch => {
  dispatch({
    type: t.SET_LIST,
    payload: list
  });
}

export const setSearchText = (search) => dispatch => {
  dispatch({
    type: t.SET_SEARCH_TEXT,
    payload: search
  });
}

export const doSearch = (search) => dispatch => {
  dispatch({
    type: t.DO_SEARCH,
    payload: search
  });
}

export const setSearchHistory = (list) => dispatch => {
  dispatch({
    type: t.SET_SEARCH_HISTORY,
    payload: list
  });
}

export const setPage = (page) => dispatch => {
  dispatch({
    type: t.SET_PAGE,
    payload: page
  });
}