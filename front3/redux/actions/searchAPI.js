import APIcall from './server';

export const getHistoryServer = (setSearchHistory) => {
    APIcall({
        method: "GET",
        url: 'history',
        events: [
            async () => {
            },
            async res => {
                await setSearchHistory(res.data.slice(1));
            }
        ]
    });
}

export const searchServer = (search, setLoading, doSearch, setList) => {
    APIcall({
        method: "GET",
        url: `search/${search}`,
        events: [
            async () => {
                await setLoading(true);
                doSearch(search)
            },
            async res => {
                await setList(res.data);
                await setLoading(false);
            }
        ]
    });
}

export const searchServerPersist = (search, setLoading, doSearch, setList) => {
    APIcall({
        method: "POST",
        url: `search/${search}`,
        events: [
            async () => {
                await setLoading(true);
                doSearch(search)
            },
            async res => {
                await setList(res.data);
                await setLoading(false);
            }
        ]
    });
}
