import React from 'react';
import styles from '../styles/List.module.css'
import Loading from 'react-loading';
import Highlighter from "react-highlight-words";

const loaderStyle = {
    color: '#cc0000',
    type: 'bars'
}
function List(props) {
    const { list, loading, onClick, highlight = [] } = props;
    return (
        <div className={styles.listContainer}>
            {loading ? <Loading {...loaderStyle} /> : list.map(item => (
                <div
                    className={styles.listItem}
                    onClick={() => onClick(item)}
                    key={item.title}
                >
                    <Highlighter
                        highlightClassName={styles.highlightClass}
                        searchWords={highlight}
                        autoEscape={true}
                        textToHighlight={item.title}
                    />
                </div>
            ))}
        </div>
    );
}

export default List;