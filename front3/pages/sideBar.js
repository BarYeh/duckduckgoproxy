import React, { useEffect } from 'react';
import styles from '../styles/Home.module.css';
import { connect } from 'react-redux';
import { setPage, doSearch, setList, setLoading, setSearchHistory } from '../redux/actions/mainActions';
import List from './list';
import { searchServerPersist, getHistoryServer } from '../redux/actions/searchAPI';

function SideBar(props) {
    const { history, setLoading, doSearch, setList, setSearchHistory } = props;
    useEffect(() => {
        getHistoryServer(setSearchHistory);
    }, []);
    return (
        <div className={styles.sideBar}>
            <List
                list={history.map(q => ({ title: q }))}
                loading={false}
                onClick={item => {
                    searchServerPersist(item.title, setLoading, doSearch, setList);
                }}
            />
        </div>
    );
}


const mapStateToProps = (state /*, ownProps*/) => {
    return {
        history: state.main.history
    };
}
  
const mapDispatchToProps = {
    setSearchHistory,
    setList,
    setLoading,
    doSearch,
    setPage
};

export default connect(mapStateToProps, mapDispatchToProps)(SideBar);