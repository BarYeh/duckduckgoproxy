import React from 'react';
import styles from '../styles/Search.module.css'
import { connect } from 'react-redux'
import { searchServer } from '../redux/actions/searchAPI';
import { setList, setLoading, setSearchText, doSearch } from "../redux/actions/mainActions"

function Search(props) {

    const doSearch = async () => {
        const { search, setLoading, doSearch, setList } = props;
        searchServer(search, setLoading, doSearch, setList);
    }
    const { search, setSearchText } = props;
    return (
        <div className={styles.searchLineContainer}>
            <input
                style={{ width: 295, height: 30, margin: 5, marginBottom: 10 }}
                id={'seach-input'}
                type="text"
                value={search}
                placeholder={'Search'}
                onChange={(comp) => setSearchText(comp.target.value)}
                onKeyDown={event => {
                    if (event.keyCode === 13) {
                        doSearch();
                    }
                }}
            />
            <div
                className={styles.searchButton}
                onClick={() => doSearch()}
            >
                <span style={{ marginTop: -2, fontSize: 14 }}>Search</span>
            </div>
        </div>
    );
}

const mapStateToProps = (state /*, ownProps*/) => {
    return {
        search: state.main.search,
        history: state.main.history
    };
}
  
const mapDispatchToProps = { setList, setLoading, setSearchText, doSearch };

export default connect(mapStateToProps, mapDispatchToProps)(Search);