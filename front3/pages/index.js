import Head from 'next/head'
import Home from './home';

const App = () => {
  return (
    <div>
      <Head>
        <title>DuckDuckGo Proxy</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Home />
    </div>
  )
}

export default App;
