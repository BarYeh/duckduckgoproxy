import React from 'react';
import styles from '../styles/Home.module.css'
import { connect } from 'react-redux'
import { searchServer } from '../redux/actions/searchAPI';
import Search from './search';
import List from './list';
import SideBar from './sideBar';
import { setPage, doSearch, setList, setLoading } from '../redux/actions/mainActions';

function Home(props) {
    const pageSize = 10;
    const navigation = () => {
        const { list, page, setPage } = props;
        const showNextPage = Math.ceil(list.length / pageSize) > page;
        const showPrevPage = page !== 1;

        const arrow = (style, cond, newPage) => (
            <div>
                <div
                    className={style}
                    style={cond ? { borderColor: 'blue' } : { borderColor: 'transparent', cursor: 'unset' }}
                    onClick={cond ? () => setPage(newPage) : null}
                />
            </div>
        );

        const prevPage = arrow(styles.arrowLeft, showPrevPage, page - 1);
        const nextPage = arrow(styles.arrowRight, showNextPage, page + 1);
        return (
            <div className={styles.navigateContainer}>
                {prevPage}
                <div className={styles.pageText}>
                    {page}
                </div>
                {nextPage}
            </div>
        );
    }

    const main = () => {
        const { list, loading, page, setLoading, doSearch, setList, search } = props;
        const pageStartIndex = (page - 1) * pageSize;
        const pageEndIndex = pageStartIndex + pageSize;
        const showList = list.slice(pageStartIndex, pageEndIndex);
        const searchs = search.split(' ');
        let appearances = 0;
        searchs.forEach((term) => {
            showList.forEach(item => {
                if (term) appearances += item.title.toLowerCase().split(term.toLowerCase()).length - 1;
            })
        });
 
        return (
            <div className={styles.mainContainer}>
                <Search />
                <div className={styles.apLabel}>Appearances: {appearances}</div>
                <List
                    highlight={searchs}
                    list={showList}
                    loading={loading}
                    onClick={item => {
                        // window.open(item.url, '_blank');
                        searchServer(item.title, setLoading, doSearch, setList);
                    }}
                />
                {navigation()}
            </div>
        );
    }
    return (
        <div className={styles.topContainer}>
            <SideBar />
            {main()}
        </div>
    );
}

const mapStateToProps = (state /*, ownProps*/) => {
    return {
        loading: state.main.loading,
        list: state.main.list,
        page: state.main.page,
        search: state.main.search
    };
}
  
const mapDispatchToProps = {
    setList,
    setLoading,
    doSearch,
    setPage
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);