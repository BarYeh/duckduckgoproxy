const axios = require('axios');
const express = require('express');
const cors = require('cors');
const fs = require('fs');
const app = express();
const port = 5000;
const filename = 'DB.csv';

app.use(cors())

app.get('/', (req, res) => {
    const send = 'Welcome to DuckDuckGo Proxy!';
    res.send(send)
})

const getDB = async () => {
    return new Promise((resolve) => {
        fs.readFile(filename, "utf-8", (err, buf) => resolve(buf || []));
    });
}

const writeToDB = async (text) => {
    return new Promise((resolve) => {
        fs.appendFile(filename, `,${text}`, () => resolve(true));
    });
}

const getSearchResult = async (search) => {
    try {
        const response = await axios.get(`https://api.duckduckgo.com/?q=${search}&format=json`);
        const reducer = (currentValue) => {
            if (currentValue.FirstURL) {
                return ([{
                    url: currentValue.FirstURL,
                    title: currentValue.Text
                }]);
            } else if (currentValue.Topics) {
                let returnValues = [];
                for (let i = 0; i < currentValue.Topics.length; i++) {
                    returnValues = [...returnValues, ...reducer(currentValue.Topics[i])]
                }
                return returnValues;
            } else return null;
        }
        let send = [];
        response.data.RelatedTopics.forEach(element => {
            send = [...send, ...reducer(element)]
        });
        return send;
    } catch (e) {
        console.log('Search error:', e);
        return ({ error: true });
    }
}

app.get('/history', async (req, res) => {
    const response = await getDB();
    res.send(response.split(','));
})

app.get('/search/*', async (req, res) => {
    const response = await getSearchResult(req.params[0]);
    res.send(response);
})

app.post('/search/*', async (req, res) => {
    const response = await getSearchResult(req.params[0]);
    const DB = await getDB();
    if (!DB.includes(req.params[0])) {
        await writeToDB(req.params[0]);
    }

    res.send(response);
});

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})